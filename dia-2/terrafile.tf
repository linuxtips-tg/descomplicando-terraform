module "servers" {
  source  = "./servers"
  servers = 1

  ## Vocẽ pode explicitar um nome caso for usar o mesmo modulo duas vezes
  # name = "servers"

  ## Chamando provider implicitamente ( do module root )
  ## Descomentar para chamar provider explicitamente
  # providers = {
  #   aws = "aws.use2"
  # }    
}

output "ip_address" {
  value = module.servers.ip_address
}

# resource "aws_route53_record" "server" {
#     zone_id = "Z01231"
#     name = "server"
#     type = "A"
#     ttl = "300"
#     records = [ module.servers.ip_address[0] ]
# }
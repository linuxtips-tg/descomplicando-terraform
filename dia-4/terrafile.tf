module "servers" {
  source  = "./servers"
  servers = var.enviroment == "production" ? 2 + var.plus : 1
}

output "count_ip_address" {
  value = module.servers.count_ip_address
}

output "for_each_ip_address" {
  value = module.servers.for_each_ip_address
}
variable "app_name" {
  type        = string
  default     = "Sem nome"
  description = "Nome da app"
}

variable "for_each_app_name" {
  type        = list(string)
  default     = ["dev", "stag", "production"]
  description = "List of app name"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "Tipo de instância ec2 usada na AWS"
}

variable "servers" {}
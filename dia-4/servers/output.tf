output "count_ip_address" {
  value = aws_instance.count_web[*].public_ip
}
output "for_each_ip_address" {
  value = {
    for instance in aws_instance.for_each_web:
    instance.id => instance.public_ip  
  }
}
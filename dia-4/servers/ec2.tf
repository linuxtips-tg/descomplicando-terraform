data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] # Ubuntu
}

resource "aws_instance" "count_web" {
  count         = var.servers
  ami           = data.aws_ami.ubuntu.id  
  instance_type = var.instance_type

  tags = {
    Name = count.index < 1 ? var.app_name : join(" ", [var.app_name, tostring(count.index)])    
  }
}

resource "aws_instance" "for_each_web" {  
  ami           = data.aws_ami.ubuntu.id
  for_each = toset(var.for_each_app_name)
  instance_type = var.instance_type

  tags = {    
    Name = each.value
  }
}

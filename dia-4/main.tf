provider "aws" {
  region = terraform.workspace == "default" ? "us-east-1" : "us-east-2"
}

terraform {
  backend "s3" {
    # Lembre de trocar o bucket para o seu, não pode ser o mesmo nome
    bucket         = "descomplicando-terraform-shibuka-tfstates"
    dynamodb_table = "terraform-state-lock-dynamo"
    key            = "terraform-test.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

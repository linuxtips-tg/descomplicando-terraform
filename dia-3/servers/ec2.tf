data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["IaaSWeek-${var.hash_commit}"]
  }

  owners = ["178520105998"] # My User
}

resource "aws_instance" "web" {
  count         = var.servers
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type

  tags = {
    Name = var.app_name
  }
}

resource "aws_instance" "web2" {
  count         = var.servers
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type

  tags = {
    Name = var.app_name
  }

  ## Dependência explicita
  depends_on = [aws_instance.web]
}

resource "aws_eip" "lb" {
  ## Dependência implicita pois depende do aws_instance.web estar instanciado
  instance = aws_instance.web.id
  vpc      = true
}
